extern crate base64;
extern crate ext_php_rs;
use ext_php_rs::exception::PhpException;
use ext_php_rs::ffi::zend_ce_exception;
use ext_php_rs::prelude::*;

use aes_gcm_siv::aead::{Aead, NewAead, Payload};
use aes_gcm_siv::{Aes256GcmSiv, Key, Nonce};

use base64::{decode, encode};

use rand::{distributions::Alphanumeric, thread_rng, Rng};

#[php_class(name = "OxyCreative\\AES256GCMSIV\\Encryption")]
pub struct Encryption {
    aes: Aes256GcmSiv,
}

fn throw_length_key_exception() -> PhpResult<Encryption> {
    return Err(PhpException::new(
        "Key must 32-byte".into(),
        1100,
        unsafe { zend_ce_exception.as_ref() }.unwrap(),
    ));
}

fn random_byte_nonce() -> String {
    thread_rng()
        .sample_iter(&Alphanumeric)
        .take(12)
        .map(char::from)
        .collect()
}

#[php_impl]
impl Encryption {
    pub fn __construct(passphrase: &str) -> PhpResult<Self> {
        if passphrase.len() != 32 {
            return throw_length_key_exception();
        }

        let passphrase = Key::from_slice(passphrase.as_bytes());
        Ok(Self {
            aes: Aes256GcmSiv::new(passphrase),
        })
    }

    pub fn encrypt(&self, plaintetxt: &str, aad: &str) -> PhpResult<String> {
        let nonce = random_byte_nonce();
        let byte_nonce = Nonce::from_slice(nonce.as_bytes());

        let payload = Payload {
            msg: plaintetxt.as_bytes(),
            aad: aad.as_bytes(),
        };

        let encrypt = self
            .aes
            .encrypt(byte_nonce, payload)
            .expect("Encryption failed");

        Ok(encode(nonce + &hex::encode(encrypt).to_string()))
    }

    pub fn decrypt(&self, b64cipher: &str, aad: &str) -> PhpResult<String> {
        let ciphertext = decode(b64cipher).expect("Failed to decode base64");
        let nonce = Nonce::from_slice(&ciphertext[..12]);
        let ciphertext = &ciphertext[12..];

        let payload = Payload {
            msg: &hex::decode(ciphertext).expect("Decryption failed"),
            aad: aad.as_bytes(),
        };

        let decrypt = self.aes.decrypt(nonce, payload).expect("Decryption failed");

        Ok(String::from_utf8(decrypt).unwrap())
    }
}

#[php_module]
pub fn get_module(module: ModuleBuilder) -> ModuleBuilder {
    module
}
