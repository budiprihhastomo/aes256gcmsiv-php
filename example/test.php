<?php

use OxyCreative\AES256GCMSIV\Encryption;

try {
    $encClass = new Encryption("8KLh421PTO1i0IZsKmYnNEaDTe6FuXVG");
    // AEAD
    $encrypt = $encClass->encrypt(json_encode(["data" => "ini rahasia ya", "timestamp" => new DateTime()]), "budiprihhastomo");
    $decrypt = $encClass->decrypt($encrypt, "budiprihhastomo");
    var_dump($decrypt);
} catch (Exception $ex) {
    var_dump($ex);
}
