<?php

// Stubs for encryption-gcm-siv

namespace OxyCreative\AES256GCMSIV {
    class Encryption {
        public function __construct(string $passphrase) {}

        public function encrypt(string $plaintetxt, string $aad): string {}

        public function decrypt(string $b64cipher, string $aad): string {}
    }
}
